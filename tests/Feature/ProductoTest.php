<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProductoTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
    	$data = $this->getData();
    	//Creamos un nuevo usuario y verificamos la respuesta.

    	$data = $this->json('POST','/user',['nombre'=>'ibuprofeno']);

    	$response = getData($data);


    	$this->post('/producto',$data)->seeJsonEquals(['created' => true]);

    	$data = $this->getData(['nombre' => 'ibuprofeno']);

    	//Actualizamos al usuario recien creado (id=1)
    	$this->put('/producto/1',$data)->seeJsonEquals(['updated'=>true]);

    	// Obtenemos los datos de dicho usuario modificado
        // y verificamos que el nombre sea el correcto
        $this->get('producto/1')->seeJson(['nombre' => 'ibuprofeno']);

        // Eliminamos al usuario
        $this->delete('producto/1')->seeJson(['deleted' => true]);

        //$this->assertTrue(true);
    }

     public function getData($custom = array())
    {
        $data = [
            'nombre'      => 'loratadina',
            'descripcion'     => 'antialergico',
            'fecha_vencimiento'  => '2017/04/24'
            ];
        $data = array_merge($data, $custom);
        return $data;
    }
}
