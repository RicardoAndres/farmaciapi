<?php

use Illuminate\Database\Seeder;
use App\Producto;
use Faker\Factory as Faker;

class ProductoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker::create();

        for ($i=0; $i < 20 ; $i++) 
        { 
        	Producto::create
        	([
        		'nombre' => $faker->unique()->word(),
        		'descripcion' => $faker->text(200),
        		'vencimiento' =>$faker->date('Y-m-d', 2020),
        		'precio' => $faker->randomNumber(6)
        	]);
        }

    }
}
