<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Almacen extends Model
{
    //

    protected $table = 'almacenes';

    protected $primaryKey = 'id';

    protected $fillable = ['almacen'];

    protected $hidden = ['created_at','updated_at'];

    public function productos_almacenes()
    {
    	return $this->hasMany('App\Producto_Almacen');
    }
}
