<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    //
    protected $table = 'productos';

    protected $fillable = ['nombre','descripcion','vencimiento', 'precio'];

    protected $hidden = ['created_at','updated_at'];

    public function productos_almacenes()
    {
    	return $this->hasMany('App\Producto_Almacen');
    }

}
