<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Producto_Almacen;
use App\Producto;
use App\Almacen;


class ProductoAlmacenController extends Controller
{
    
    //
    public function index($id_a)
    {
    	$almacen = Almacen::find($id_a);
	    	if (!$almacen) {
	    		return response()->json(['Mensaje' => 'Almacen No Encontrado', 'Codigo' => 404], 404);
	    	}else{
				$busq = DB::table('producto_almacen')->where('almacen_id',$id_a)->get();

				if ($busq->isEmpty()) {
	    			return response()->json(['Mensaje' => 'El almacen '.$almacen->almacen.' no posee productos', 'Codigo' => 404], 404);	
				}
				else {
					$search = DB::table('producto_almacen')->where('almacen_id', $id_a)->pluck('producto_id');
					$search = $search->toArray();
					#$prod = Producto::whereIn('id',$search)->get();
					$prod = DB::table('productos')->whereIn('id', $search )->get();
	   				return response()->json(['Datos' => ['Almacen:' => $almacen->almacen , 'Productos:' => $prod], 'Codigo' => 200], 200);
				}
	    	}
    }



    public function store(Request $request, $id_a)
    {
    	$almacen = Almacen::find($id_a);
    	if (!$almacen) {
    		return response()->json(['Mensaje' => 'Almacen No Encontrado', 'Codigo' => 404], 404);
    	} 
    	else {
    			$producto = new Producto();
				$search = DB::table('productos')->where('nombre',$request->nombre)->first();
    		if (!$search) {
    			$producto->nombre = $request->nombre;
				$producto->descripcion = $request->descripcion;
				$producto->precio = $request->precio;
    			$producto->save();  			
    		}
	            $alm_prod = new Producto_Almacen();

				$busq = DB::table('producto_almacen')->where('almacen_id',$id_a)->where('producto_id',$search->id)->where('vencimiento',$request->vencimiento)->first();


			if (!$busq) {
	            $alm_prod->producto_id = $search->id;
	            $alm_prod->almacen_id = $id_a;
	            $alm_prod->vencimiento = $request->vencimiento;
	            $alm_prod->stock = 1;
	            $alm_prod->save();
			} 
			else {
				$stock = $busq->stock + 1;
				DB::table('producto_almacen')->where('id', $busq->id)->update(['stock' => $stock ]);
			}
    			return response()->json(['Mensaje' => 'Se ha Registrado el Producto: '.$request->nombre.' En el almacen: '.$almacen->almacen , 'Codigo' => 200], 200);
       	}
    }




    public function show($id_a, $id_p)
    {
    	$almacen = Almacen::find($id_a);
    	$producto = Producto::find($id_p);

    	if ((!$almacen) || (!$producto)) {
    		return response()->json(['Mensaje' => 'Almacen o Producto No Encontrado', 'Codigo' => 404], 404);
    	}
    	else{
			$busq = DB::table('producto_almacen')->where('almacen_id',$id_a)->where('producto_id',$id_p)->get();

			if ($busq->isEmpty()) {
    			return response()->json(['Mensaje' => 'El producto '.$producto->nombre.' no se encuentra en el almacen '.$almacen->almacen, 'Codigo' => 404], 404);	
			}
			else{
    			return response()->json(['Mensaje' => $busq->all(), 'Codigo' => 200], 200);

    			#return response()->json(['Mensaje' => 'El producto '.$producto->nombre.' se encuentra en el almacen '.$almacen->almacen.'. Quedan '.$busq->stock.' en stock'.'con fecha de vencimiento: '.$busq->vencimiento, 'Codigo' => 200], 200);
			}
    	}
    }


    public function update(Request $request, $id_a, $id_p)
    {
    	$almacen = Almacen::find($id_a);
    	$producto = Producto::find($id_p);
    	if ((!$almacen)||(!$producto)) {
    		return response()->json(['Mensaje' => 'Almacen o Producto No Encontrado', 'Codigo' => 404], 404);
    	}
    	else {

			$busq = DB::table('producto_almacen')->where('almacen_id',$id_a)->where('producto_id',$id_p)->first();

			if (!$busq) {
    			return response()->json(['Mensaje' => 'El producto '.$producto->nombre.' no se encuentra en el almacen '.$almacen->almacen, 'Codigo' => 404], 404);	
			}
			else {

				

						$search = DB::table('productos')->where('nombre',$request->nombre)->first();
						$busq = DB::table('producto_almacen')->where('almacen_id',$id_a)->where('producto_id',$id_p)->where('vencimiento',$request->b_vencimiento)->first();

					if ($busq) {
						if ($busq->stock == 0) {
    						return response()->json(['Mensaje' => 'No quedan '.$producto->nombre.' en el stock del almacen '.$almacen->almacen, 'Codigo' => 404], 404);							
						} else {
							$stock = $busq->stock - 1;
							DB::table('producto_almacen')->where('id', $busq->id)->update(['stock' => $stock ]);
						}
					}

					if (!$search) {
						$prod = new Producto();
						$prod->nombre = $request->nombre;
						$prod->descripcion = $request->descripcion;
						$prod->precio = $request->precio;
						$prod->save();

					}
					else {
						$prod = Producto::find($search->id);
						$prod->nombre = $request->nombre;
						$prod->descripcion = $request->descripcion;
						$prod->precio = $request->precio;
						$prod->save();
							
					}
						$busq = DB::table('producto_almacen')->where('almacen_id',$id_a)->where('producto_id',$search->id)->where('vencimiento',$request->vencimiento)->first();

						if (!$busq) {
							$alm_prod = new Producto_Almacen();
			            	$alm_prod->almacen_id = $id_a;
							$alm_prod->producto_id = $search->id;
							$alm_prod->vencimiento = $request->vencimiento;
							$alm_prod->stock = 1;
			            	$alm_prod->save();
						}
						else {
							$stock = $busq->stock + 1;
							DB::table('producto_almacen')->where('id', $busq->id)->update(['stock' => $stock ]);
						}

					return response()->json(['Mensaje' => 'Producto '.$request->nombre.' Actualizado en el almacen '.$almacen->almacen, 'Codigo' => 200], 200);
			}

		}
    }


    public function destroy($id_a, $id_p)
    {
    	$almacen = Almacen::find($id_a);
    	$producto = Producto::find($id_p);
    	if ((!$almacen)||(!$producto)) {
    		return response()->json(['Mensaje' => 'Almacen o Producto No Encontrado', 'Codigo' => 404], 404);
    	}
    	else {
			$busq = DB::table('producto_almacen')->where('almacen_id',$id_a)->where('producto_id',$id_p)->first();

			if (!$busq) {
    			return response()->json(['Mensaje' => 'No se encuentra el producto'.$producto->nombre.'en el almacen '.$almacen->almacen, 'Codigo' => 404], 404);	
			}
			else {
				if ($busq->stock == 0) {
	    			$alm = Producto_Almacen::find($busq->id);
	    			$alm->delete();	
	    			
	    			return response()->json(['Mensaje' => 'El producto '.$producto->nombre.' en el almacen '.$almacen->almacen.', se encuenta agotado', 'Codigo' => 404], 404);	
				}
				else {
					$stock = $busq->stock - 1;
					DB::table('producto_almacen')->where('id', $busq->id)->update(['stock' => $stock ]);
					return response()->json(['Mensaje' => 'Producto Facturado', 'Codigo' => 200], 200);
				}
			}
    	}
    }

}