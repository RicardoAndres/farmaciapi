<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Producto;
use App\Producto_Almacen;
use App\Http\Requests\ProductoRequest;


class ProductoController extends Controller
{
    //Recuerda que cuando creamos una API es con la finalidad de que sea accesible desde otro programa, por lo cual métodos como create, update (GET) no son necesarios ya que estos se encargan sólo de mostrar un formulario o una vista, por lo cual tal vez la mejor opción sería excluir estas rutas

    

    public function index()
    {
    	return response()->json(['datos' => Producto::all()],200);

    }


 
}
