<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Almacen;
use App\Producto_Almacen;

class AlmacenController extends Controller
{
    //
    public function index()
    {
    	return response()->json(['Datos' => Almacen::all()], 200);
    }

    public function store(Request $request)
    {
        $nom_alm = $request->almacen;
        
        $search = DB::table('almacenes')->where('almacen',$request->almacen)->first();
        
        if (!empty($search)) {
            $almacen = $search->almacen;  
        }

        if (!empty($almacen)) {
                return response()->json(['Mensaje' => 'El almacen ya existe!!!!', 'Codigo' => 404], 404);   
        }
        else {
            $almacen = new Almacen($request->all());
            $almacen->save();
            return response()->json(['Mensaje' => 'Almacen Guardado', 'Codigo' => 200], 200);
        }
    }


    public function show($id)
    {
        $almacen = Almacen::find($id);
        if (!$almacen) {
            return response()->json(['Mensaje' => 'Almacen No Encontrado', 'Codigo' => 404], 404);
        
        } else
        {
          return response()->json(['Mensaje' => $almacen->almacen, 'Codigo' => 200], 200);
        }
    }

    public function update(Request $request, $id)
    {
        $almacen = Almacen::find($id);
        if (!$almacen) {
            return response()->json(['Mensaje' => 'Almacen No Encontrado', 'Codigo' => 404], 404);
        } else
        {
          $almacen->fill($request->all());
          $almacen->save();
          return response()->json(['Mensaje' => 'Almacen Actualizado', 'Codigo' => 200], 200);
        }

    }

    public function destroy($id)
    {
        $almacen = Almacen::find($id);
        if (!$almacen) {
            return response()->json(['Mensaje' => 'Almacen No Encontrado', 'Codigo' => 404], 404);
        
        } else
        {
          $almacen->delete();
		  return response()->json(['Mensaje' => 'Almacen Eliminado', 'Codigo' => 200], 200);
        }
    }

}
