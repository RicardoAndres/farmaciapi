<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto_Almacen extends Model
{
    //

    protected $table = 'producto_almacen';

    protected $fillable = ['producto_id', 'almacen_id', 'stock'];

    protected $hidden = ['created_at','updated_at'];

    public function producto()
    {
    	return $this->belongsTo('App\Producto');
    }

    public function almacen()
    {
    	return $this->belongsTo('App\Almacen');
    }
}
